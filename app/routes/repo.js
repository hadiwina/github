import Ember from 'ember';
import Repo from 'github/models/repo';

export default Ember.Route.extend({
  model({owner, repo}) {
    return Ember.RSVP.hash({
      repo: this.store.findRecord('repo', `${owner}/${repo}`),
      attributes: Ember.get(Repo, 'attributes')
    });
  }
});
